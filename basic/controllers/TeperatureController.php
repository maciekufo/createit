<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\savetodb;

class TemperatureController extends Controller
{
    public function actionIndex()
    {
        $query = Temperature::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $temperatures = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('dbview', [
            'temperatures' => $temperatures,
            'pagination' => $pagination,
        ]);
    }
}