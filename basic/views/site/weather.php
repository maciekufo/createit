<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Json;
use app\models\TempDownload;
use app\models\Temperature;
use yii\Caching\Cache;



        $this->title = 'Weather';
        $this->params['breadcrumbs'][] = $this->title;
        ?>
        <div class="site-contact">
            <h1><?= Html::encode($this->title) ?></h1>

            <?php if (Yii::$app->session->hasFlash('weatherFormSubmitted')): ?>

                <div class="alert alert-success">
                <?php 
        

        $city=$model->getcity();
        Echo "<br> City ".$city."<br> ";

                $temp1= new TempDownload();
                    
                $tempgettemp=$temp1->gettemp($city);
                //Echo " Temperature from openweathermap.org ".$tempgettemp."";

                $temp2= new TempDownload();

                $tempgettemp2=$temp2->gettemp2($city);
            // Echo "<br> Temperature from weatherapi.com  ".$tempgettemp2."";
                

                $avrtemp= new TempDownload();
                    $avrtemp1=$avrtemp->avrtemp($tempgettemp,$tempgettemp2);
            if($tempgettemp==null or $tempgettemp2==null)

            {
                echo "No data for city: ".$city."";
            }else{
                Echo "<br>  Temperature from openweathermap.org ".$tempgettemp."";
                Echo "<br> Temperature from weatherapi.com  ".$tempgettemp2."";
                Echo "<br> Avarage temp = ".$avrtemp1."";
            }
                $temperature= new Temperature();
                $temperature->savetodb($tempgettemp,$tempgettemp2,$city);
                ?>
                
                </div>


            <?php else: ?>

            
                <div class="row">
                    <div class="col-lg-5">

                        <?php $form = ActiveForm::begin(['id' => 'weather-form']); ?>

                            <?= $form->field($model, 'city')->textInput(['autofocus' => true]) ?>

                            <div class="form-group">
                                <?= Html::submitButton('Post', ['class' => 'btn btn-primary', 'name' => 'weather-button']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

            <?php endif; ?>
        </div>
    
        