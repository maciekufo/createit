<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Countries</h1>
<ul>
<?php foreach ($teperatures as $Temperature): ?>
    <li>
        <?= Html::encode("{$Temperature->temp1} ({$Temperature->temp2})") ?>:
        <?= $Temperature->city ?>
    </li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>