<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class WeatherForm extends Model
{
    public $country;
    public $city;
    public $verifyCode;

public function getcity(){
   
    $city=$this->city;
    return $city;
}
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['country', 'city'],'required'],
            //[['country', 'city'],'name'], Zrobić validację danych
            // email has to be a valid email address
           // ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    public function weather()
    {
        return $this->city;
        
        
    }
    
}
